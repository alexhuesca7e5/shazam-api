package usecases

import model.musicData.Artist
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class DeezerUseCasesTest {

    @BeforeEach
    fun setUp() {
    }

    @Test
    fun showArtist() {
        val list = listOf(Artist("https://www.deezer.com/artist/13","Eminem","https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/250x250-000000-80-0-0.jpg"))
        val expect = listOf(Artist("https://www.deezer.com/artist/13","Eminem","https://e-cdns-images.dzcdn.net/images/artist/19cc38f9d69b352f718782e7a22f9c32/250x250-000000-80-0-0.jpg"))

        assertEquals(expect,list)
    }

    @Test
    fun showAlbum() {
    }

    @Test
    fun showTrack() {
    }

    @Test
    fun getDeezerRepository() {
    }
}