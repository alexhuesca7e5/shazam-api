package usecases

import DeezerRepository
import java.time.LocalTime

class DeezerUseCases(val deezerRepository: DeezerRepository = DeezerRepository()) {
    suspend fun showArtist(name: String): String {
        val apiResponse = deezerRepository.searchArtist(name).data[0]
        val artistName = apiResponse.name.parseName()
        val artistLink = apiResponse.link
        return "[$artistName]($artistLink)"
    }
    suspend fun showAlbum(name:String): Pair<String, List<Pair<String, String>>> {
        val apiResponse = deezerRepository.search(name).data[0]
        val albumTitle = apiResponse.album.title.parseName()
        val cover = apiResponse.album.cover_xl
        val songs = showAlbumTracks(apiResponse.album.tracklist)
        return "[$albumTitle]($cover)" to songs
    }
    suspend fun showTrack(name:String): Pair<String, String> {
        val apiResponse = deezerRepository.search(name).data[0]
        val trackTitle = apiResponse.title
        val album = apiResponse.album.title
        val artist = apiResponse.artist.name.parseName()
        val artistLink = apiResponse.artist.link
        val rank = apiResponse.rank
        val preview = apiResponse.preview
        return "$trackTitle\nAlbum: $album\nRank: $rank\nArtist: [$artist]($artistLink)" to preview
    }
    private suspend fun showAlbumTracks(albumTracksURL: String):List<Pair<String, String>>{
        val apiResponse = deezerRepository.getAlbumSongs(albumTracksURL).data.sortedBy { it.track_position }
        val trackList: MutableList<Pair<String, String>> = mutableListOf()
        apiResponse.forEach {
            val time = LocalTime.ofSecondOfDay(it.duration.toLong())
            val trackTitle = it.title.parseName()
            val duration = "${(time.minute.toString()).padStart(2,'0')}:${(time.second.toString()).padStart(2, '0')}"
            val preview = it.preview
            trackList.add("[$trackTitle]($preview)\n$duration" to preview)
        }
        return trackList
    }
    suspend fun playTrack(name: String): Pair<String, String> {
        val apiResponse = deezerRepository.search(name).data[0]
        val title = apiResponse.title.parseName()
        val link = apiResponse.link
        val preview = apiResponse.preview
        return "[$title]($link)" to preview
    }
    private fun String.parseName(): String {
        var title = ""
        for(char in this){
            if (char == '(' || char == ')' || char == '.' || char == '&' || char == '!' || char == '¡' || char == '¿' || char == '?' || char == '$' || char == '-'
            ){
                continue
            }
            title += char
        }
        return title
    }
}