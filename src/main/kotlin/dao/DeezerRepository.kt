import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import model.musicData.MusicStorage
import model.tracks.Tracks
import model.artistExtended.Artist

class DeezerRepository() {
    val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    suspend fun search(name: String): MusicStorage =
        client.get("https://api.deezer.com/search/") {
            parameter("q", name)
            parameter("limit", 1)
        }.body()
    suspend fun searchArtist(artistName: String): Artist =
        client.get("https://api.deezer.com/search/artist/") {
            parameter("q", artistName)
            parameter("limit", 1)
        }.body()
    suspend fun getAlbumSongs(albumTracksURL: String): Tracks = client.get(albumTracksURL) {
    }.body()

}