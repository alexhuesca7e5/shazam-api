package model.artistExtended

@kotlinx.serialization.Serializable
data class Artist(
    val `data`: List<DataExtended>,
    val total: Int
)