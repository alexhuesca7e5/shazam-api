package model.tracks

@kotlinx.serialization.Serializable
data class Tracks(
    val `data`: List<Data>,
    val total: Int
)