package model.musicData

@kotlinx.serialization.Serializable
data class MusicStorage(
    val `data`: List<Data>,
    val next: String,
    val total: Int
)