package model

import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.callbackQuery
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.text
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.InlineKeyboardMarkup
import com.github.kotlintelegrambot.entities.ParseMode
import com.github.kotlintelegrambot.entities.keyboard.InlineKeyboardButton
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import usecases.DeezerUseCases

@OptIn(DelicateCoroutinesApi::class)
fun main() {
    val deezerUseCases: DeezerUseCases = DeezerUseCases()
    val bot = bot {
        token = "5711026572:AAGqe1KOKDGg4kYHcWogftfq_-uFQbEHg3o"
        dispatch {
            var any:Any = ""
            command("start"){
                bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Type /commands to see all the commands")
            }
            command("commands"){
                bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text =
                        "/artist: Te muestra información sobre el artista \n" +
                        "/album: Te muestra información del album \n" +
                        "/track: Te muestra información de la canción \n" +
                        "/play: Reproduce la canción")
            }
            command("artist") {
                val userInput = args.joinToString(" ")
                GlobalScope.launch {
                    val data = deezerUseCases.showArtist(userInput)
                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = data, ParseMode.MARKDOWN_V2)
                }
            }
            command("album") {
                val userInput = args.joinToString(" ")
                GlobalScope.launch {
                    val data = deezerUseCases.showAlbum(userInput)
                    val album = data.first
                    any = data.second
                    val inlineKeyboardMarkup = InlineKeyboardMarkup.create(
                        listOf(
                            InlineKeyboardButton.CallbackData(text = "Si", callbackData = "Yes"),
                            InlineKeyboardButton.CallbackData(text = "No", callbackData = "No")
                        )
                    )
                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = album, ParseMode.MARKDOWN_V2)
                    bot.sendMessage(
                        ChatId.fromId(message.chat.id),
                        "Quieres ver sus canciones?",
                        replyMarkup = inlineKeyboardMarkup
                    )
                }
            }
            callbackQuery("Yes") {
                val chatId = callbackQuery.message?.chat?.id ?: return@callbackQuery
                (any as  List<Pair<String, String>>).forEach {
                    bot.sendMessage(ChatId.fromId(chatId), it.first, ParseMode.MARKDOWN_V2)
                    bot.sendAudio(ChatId.fromId(chatId), it.second)
                }
                bot.deleteMessage(
                    ChatId.fromId(chatId),
                    callbackQuery.message?.messageId ?: return@callbackQuery
                )
            }
            callbackQuery("No") {
                val chatId = callbackQuery.message?.chat?.id ?: return@callbackQuery
                bot.deleteMessage(
                    ChatId.fromId(chatId),
                    callbackQuery.message?.messageId ?: return@callbackQuery
                )
            }
            command("track") {
                val userInput = args.joinToString(" ")
                GlobalScope.launch {
                    val data = deezerUseCases.showTrack(userInput)
                    bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = data.first, ParseMode.MARKDOWN_V2)
                    bot.sendAudio(ChatId.fromId(message.chat.id), data.second)
                }
            }
            command("play"){
                val userInput = args.joinToString(" ")
                GlobalScope.launch {
                    val data = deezerUseCases.playTrack(userInput)
                    bot.sendMessage(ChatId.fromId(message.chat.id), data.first, ParseMode.MARKDOWN_V2)
                    bot.sendAudio(ChatId.fromId(message.chat.id), data.second)
                }
            }
        }
    }
    bot.startPolling()
}